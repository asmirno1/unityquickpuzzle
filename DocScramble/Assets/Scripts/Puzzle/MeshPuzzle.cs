﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshPuzzle : MonoBehaviour {

    protected static int MAXPHYSICSITERATIONS = 500; // In case a piece gets stuck

	// Use this for initialization
	protected virtual void Start () {
        this.SetupPuzzlePieces();
        this.PlacePiecesRandomly();
	}

    protected bool kinematicPieces = false; // includes gravity

    protected List<MeshPuzzlePiece> pieces = new List<MeshPuzzlePiece>();

    protected class ActivePiece{
        public MeshPuzzlePiece piece;
        protected Vector3 handleOffset;
        protected Quaternion initialRotation;
        protected static float minLiftAboveLowerBound = PuzzleManager.Instance.puzzleArea.min.y + 0.05f;

        public ActivePiece(MeshPuzzlePiece piece, Vector3 handle){
            this.piece = piece;
            this.handleOffset = handle - piece.transform.position;
            this.initialRotation = piece.transform.rotation;
        }

        public void TranslateTo(Vector3 destination){
            Vector3 h = new Vector3(destination.x - this.handleOffset.x, Mathf.Max(destination.y, ActivePiece.minLiftAboveLowerBound), destination.z - this.handleOffset.z);
            this.piece.transform.position = Vector3Util.Clamp(h, PuzzleManager.Instance.puzzleArea.min, PuzzleManager.Instance.puzzleArea.max);
        }

        public void RotateAroundY(float degrees){
            //this.piece.transform.RotateAround(this.piece.transform.position, Vector3.up, degrees);
            this.piece.transform.Rotate(0f, degrees, 0f, Space.World);
        }
    }

    protected ActivePiece activePiece;
	
    protected virtual void SetupPuzzlePieces(){
        foreach (Transform childTransform in this.transform)
        {
            GameObject child = childTransform.gameObject;
            MeshPuzzlePiece childMPP = child.AddComponent<MeshPuzzlePiece>();
            this.SetupChildPuzzlePiece(childMPP);
            pieces.Add(childMPP);
        }
    }

    protected virtual void SetupChildPuzzlePiece(MeshPuzzlePiece piece){
        piece.gameObject.AddComponent<Rigidbody>().isKinematic = this.kinematicPieces;
        piece.gameObject.AddComponent<MeshCollider>().convex = true;
        piece.gameObject.GetComponent<MeshCollider>().cookingOptions =
            MeshColliderCookingOptions.WeldColocatedVertices & MeshColliderCookingOptions.EnableMeshCleaning;
        //!important DO NOT use MeshColliderCookingOptions.CookForFasterSimulation (on by default)
        piece.parentPuzzle = this;
        piece.gameObject.layer = PuzzleManager.PUZZLEPIECELAYER;
    }

    public virtual void ResetPiecesToInitialPosition(){
        foreach(MeshPuzzlePiece piece in this.pieces){
            piece.ResetToInitialPosition();
        }
    }

    public virtual void PlacePiecesRandomly(){
        Bounds placementBounds = PuzzleManager.Instance.puzzleArea;
        this.RandomPiecePlacement(placementBounds);
        this.SimulatePiecePhysicsUntilRest();
    }

    protected virtual void RandomPiecePlacement(Bounds enclosingBounds){
        foreach(MeshPuzzlePiece piece in this.pieces){
            piece.RandomPlacement(enclosingBounds, true);
        }
    }

    public virtual void SimulatePiecePhysicsUntilRest(){
        if(this.pieces.Count == 0){
            return;
        }
        Physics.autoSimulation = false;
        foreach(MeshPuzzlePiece piece in this.pieces){
            piece.gameObject.GetComponent<Rigidbody>().isKinematic = false;
        }
        for (int i = 0; i < MAXPHYSICSITERATIONS; ++i){
            if(this.pieces.TrueForAll(MeshPuzzlePiece.RigidbodyIsSleeping)){
                break;
            }
            Physics.Simulate(Time.fixedDeltaTime);
        }
        foreach (MeshPuzzlePiece piece in this.pieces)
        {
            piece.gameObject.GetComponent<Rigidbody>().isKinematic = this.kinematicPieces;
        }
        Physics.autoSimulation = true;
    }


    // Functions for updating position of puzzles

    public virtual void DeselectActivePiece(){
        if (this.activePiece != null)
        {
            this.activePiece.piece.Deselect();
            this.activePiece = null;
        }
    }

    public virtual void SelectActivePiece(MeshPuzzlePiece piece, Vector3 selectHandle){
        this.activePiece = new ActivePiece(piece, selectHandle);
        piece.Select();
    }

    public virtual void HandleInput(Ray mousePositionRay, Dictionary<KeyCode, bool> keyActiveDict, Dictionary<KeyCode, bool> keyDownDict, Dictionary<KeyCode, bool> keyUpDict, float vertAxis, float horixAxis){

        // Meta Actions
        bool resetPuzzle = false;
        if(keyUpDict.TryGetValue(KeyCode.R, out resetPuzzle)){
            if (resetPuzzle)
            {
                this.ResetPiecesToInitialPosition();
            }
        }
        bool randomizePuzzle = false;
        if(keyUpDict.TryGetValue(KeyCode.Space, out randomizePuzzle)){
            if (randomizePuzzle)
            {
                this.PlacePiecesRandomly();
            }
        }

        // Puzzle Select/ Deselect
        bool leftMouseReleased = false;
        if (keyUpDict.TryGetValue(KeyCode.Mouse0, out leftMouseReleased))
        {
            if (leftMouseReleased)
            {
                this.DeselectActivePiece();
            }
        }

        if (this.activePiece != null)
        {
            Plane piecePlane = new Plane(Vector3.up, this.activePiece.piece.transform.position);
            Vector3 nextLocation = Vector3Util.RayPlaneIntersect(mousePositionRay, piecePlane);
            if (nextLocation != Vector3.positiveInfinity)
            {
                this.activePiece.TranslateTo(nextLocation + new Vector3(0f, vertAxis * PuzzleManager.Instance.translateAxisMultiplier, 0f));
                this.activePiece.RotateAroundY(horixAxis * PuzzleManager.Instance.pieceRotationSpeed);
            }
        }
    }
}
