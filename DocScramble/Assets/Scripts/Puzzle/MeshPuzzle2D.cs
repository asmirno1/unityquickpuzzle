﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshPuzzle2D : MeshPuzzle {

    protected override void SetupChildPuzzlePiece(MeshPuzzlePiece piece)
    {
        base.SetupChildPuzzlePiece(piece);
        // https://docs.unity3d.com/ScriptReference/Rigidbody-constraints.html
        // Removed for the moment, not interacting well with collisions
        //piece.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
        piece.GetComponent<Rigidbody>().collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;
    }

    protected override void RandomPiecePlacement(Bounds enclosingBounds)
    {
        foreach (MeshPuzzlePiece piece in this.pieces)
        {
            piece.transform.Rotate(0f, Random.Range(0f, 360f), 0f, Space.World);
            piece.SetLocation(BoundsUtil.RandomCenterWithinBounds(enclosingBounds, piece.gameObject.GetComponent<Collider>().bounds));
        }
    }

}
