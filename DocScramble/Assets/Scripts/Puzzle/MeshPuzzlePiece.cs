﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshPuzzlePiece : MonoBehaviour {

    public MeshPuzzle parentPuzzle; // TODO: Use a factory design pattern and make this value readonly
    public Bounds initialBounds { get; private set; }
    public Vector3 initialposition { get; private set; }
    public Quaternion initialRotation { get; private set; }
    public Color initialColor { get; private set; }

    private bool isSelected = false;
    private Vector3 selectedInitialStateRotation;

    public MeshPuzzlePiece(MeshPuzzle parentPuzzle){
        this.parentPuzzle = parentPuzzle;
    }

    public static bool RigidbodyIsSleeping(MeshPuzzlePiece piece)
    {
        return piece.GetComponent<Rigidbody>().IsSleeping();
    }

    void Awake () {
        this.initialposition = this.gameObject.transform.position;
        this.initialRotation = this.gameObject.transform.rotation;
        this.initialBounds = this.gameObject.GetComponent<Renderer>().bounds;
        this.initialColor = this.gameObject.GetComponent<Renderer>().material.color;
    }

    public void ResetToInitialPosition()
    {
        this.transform.position = this.initialposition;
        this.transform.rotation = this.initialRotation;
        this.gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
    }

    public void SetLocation(Vector3 location)
    {
        this.gameObject.transform.position = location;
    }

    public void SetRotation(Quaternion rotation){
        this.gameObject.transform.rotation = rotation;
    }

    public Bounds RotateGetNewBounds(Quaternion rotation){
        this.SetRotation(rotation);
        return this.GetComponent<Renderer>().bounds;
    }

    public void RandomPlacement(Bounds enclosing, bool randomRotate=true){
        Bounds selfBounds = randomRotate ? this.RotateGetNewBounds(Random.rotationUniform) : this.gameObject.GetComponent<Collider>().bounds;
        this.SetLocation(BoundsUtil.RandomCenterWithinBounds(enclosing, selfBounds));
    }

    // Selection or picking

    public void InformRootPuzzleOfSelection(Vector3 grabHandle){
        this.parentPuzzle.SelectActivePiece(this, grabHandle);
    }

    public void Select()
    {
        this.isSelected = true;
        this.GetComponent<Rigidbody>().useGravity = false;
        this.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;

        this.selectedInitialStateRotation = this.transform.rotation.eulerAngles;
        this.gameObject.GetComponent<Renderer>().material.color = this.initialColor*1.5f;
    }

    public void Deselect()
    {
        this.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
        this.isSelected = false;
        this.GetComponent<Rigidbody>().useGravity = true;
        this.gameObject.GetComponent<Renderer>().material.color = this.initialColor;
    }

    // Temporary short-term fix for errant rotation while I figure out why freezing rigidbody rotation does not work
    private void Update()
    {
        if (this.parentPuzzle.gameObject.GetComponent<MeshPuzzle2D>()!=null && this.isSelected){
            this.transform.eulerAngles = new Vector3(this.selectedInitialStateRotation.x, this.transform.eulerAngles.y, this.selectedInitialStateRotation.z);
        }
    }
}
