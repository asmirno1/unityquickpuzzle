﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PuzzleManager : MonoBehaviour {

    // Singleton Design Pattern
    public static PuzzleManager Instance { get; private set; }

    public static int PUZZLEPIECELAYER = 9;

    public Bounds puzzleArea;

    public float translateAxisMultiplier = 0.01f;
    public float pieceRotationSpeed = 3f;

    void Awake () {
        if(Instance == null)
        {
            Instance = this;
        }
	}

    private void Start()
    {
        Collider enclosingCollider = this.CreateEnclosingColliderBox(this.puzzleArea);
    }

    protected Collider CreateEnclosingColliderBox(Bounds bounds)
    {
        GameObject cubeObject = GameObject.CreatePrimitive(PrimitiveType.Cube);
        cubeObject.transform.localScale = 2f * bounds.extents;
        cubeObject.transform.position = bounds.center;
        Mesh cubeMesh = cubeObject.GetComponent<MeshFilter>().mesh;
        // https://forum.unity.com/threads/can-you-invert-a-sphere-or-box-collider.118733/
        cubeMesh.triangles = cubeMesh.triangles.Reverse().ToArray();
        BoxCollider bc = cubeObject.GetComponent<BoxCollider>();
        if (bc)
        {
            DestroyImmediate(bc);
        }
        cubeObject.GetComponent<MeshRenderer>().enabled = false;
        return cubeObject.AddComponent<MeshCollider>();
    }

    public bool RaycastMousehitToPuzzlePieces(Ray mousePositionRay)
    {
        float maxRaycastDistance = Vector3.Distance(mousePositionRay.origin, PuzzleManager.Instance.puzzleArea.center + PuzzleManager.Instance.puzzleArea.extents) + 10f;
        RaycastHit hit;
        // Tilde (negate) flips the layermask to only raycast hit puzzle pieces
        if(Physics.Raycast(mousePositionRay, maxDistance: maxRaycastDistance, layerMask: ~PuzzleManager.PUZZLEPIECELAYER, hitInfo: out hit)){
            hit.rigidbody.gameObject.GetComponent<MeshPuzzlePiece>().InformRootPuzzleOfSelection(hit.point);
            return true;
        }
        return false;
    }
}
