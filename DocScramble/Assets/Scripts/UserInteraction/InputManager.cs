﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour {

    public MeshPuzzle[] activePuzzles;

    // Singleton Design Pattern
    public static InputManager Instance { get; private set; }

    private static string VERTICALAXIS = "Vertical";
    private static string HORIZONTALAXIS = "Horizontal";

    void Awake () {
        if(Instance == null)
        {
            Instance = this;
        }
	}
	
	// Update is called once per frame
	void Update () {
        float vertAxis = Input.GetAxis(VERTICALAXIS);
        float horizAxis = Input.GetAxis(HORIZONTALAXIS);
        Ray mouseRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        Dictionary<KeyCode, bool> keyActiveDict = new Dictionary<KeyCode, bool>{
            {KeyCode.Comma, Input.GetKey(KeyCode.Comma)},
            {KeyCode.Period, Input.GetKey(KeyCode.Period)},
            {KeyCode.Mouse0, Input.GetKey(KeyCode.Mouse0)}
        };
        Dictionary<KeyCode, bool> keyUpDict = new Dictionary<KeyCode, bool>{
            {KeyCode.R, Input.GetKeyUp(KeyCode.R)},
            {KeyCode.Space, Input.GetKeyUp(KeyCode.Space)},
            {KeyCode.Mouse0, Input.GetKeyUp(KeyCode.Mouse0)}
        };
        Dictionary<KeyCode, bool> keyDownDict = new Dictionary<KeyCode, bool>{
            {KeyCode.Mouse0, Input.GetKeyDown(KeyCode.Mouse0)}
        };

        bool leftMousePressed = false;
        if (keyDownDict.TryGetValue(KeyCode.Mouse0, out leftMousePressed))
        {
            if (leftMousePressed)
            {
                PuzzleManager.Instance.RaycastMousehitToPuzzlePieces(mouseRay);
            }
        }
        foreach(MeshPuzzle puzzle in this.activePuzzles){
            puzzle.HandleInput(mouseRay, keyActiveDict, keyDownDict, keyUpDict, vertAxis, horizAxis);
        }
    }

    public void ResetActivePuzzles()
    {
        foreach (MeshPuzzle puzzle in activePuzzles)
        {
            puzzle.ResetPiecesToInitialPosition();
        }
    }

    public void RandomizeActivePuzzles()
    {
        foreach (MeshPuzzle puzzle in activePuzzles)
        {
            puzzle.PlacePiecesRandomly();
        }
    }
}


