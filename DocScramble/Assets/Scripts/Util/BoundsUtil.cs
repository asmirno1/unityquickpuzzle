﻿using UnityEngine;

public static class BoundsUtil
{

    public static bool BoundsWithingBounds(Bounds enclosing, Bounds within)
    {
        return enclosing.Contains(within.min) && enclosing.Contains(within.max);
    }

    // Warning: if within is larger in any dimension than enclosing, the resulting extents vector will be
    // negative in that dimension so the Bounds constructor will likely set it to 0
    public static Bounds GetCenterPointBounds(Bounds enclosing, Bounds within){
        return new Bounds(enclosing.center, enclosing.extents - within.extents);
    }

    public static Vector3 RandomPointInBounds(Bounds bounds){
        return Vector3Util.RandomVectorInRange(bounds.min, bounds.max);
    }

    public static Vector3 RandomCenterWithinBounds(Bounds enclosing, Bounds within){
        Bounds centerPointBounds = GetCenterPointBounds(enclosing, within);
        return RandomPointInBounds(centerPointBounds);
    }

}
