﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Vector3Util {

    public static Vector3 RandomVectorInRange(Vector3 min, Vector3 max){
        return new Vector3(Random.Range(min.x, max.y), Random.Range(min.y, max.y), Random.Range(min.z, max.z));
    }

    public static Vector3 Clamp(Vector3 toClamp, Vector3 min, Vector3 max){
        return new Vector3(Mathf.Clamp(toClamp.x, min.x, max.x), Mathf.Clamp(toClamp.y, min.y, max.y), Mathf.Clamp(toClamp.z, min.z, max.z));
    }

    public static Vector3 RayPlaneIntersect(Ray ray, Plane plane){
        float enter;
        return plane.Raycast(ray, out enter) ? ray.GetPoint(enter) : Vector3.positiveInfinity;
    }
}
